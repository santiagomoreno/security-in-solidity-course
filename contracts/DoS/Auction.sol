// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./IAuction.sol";

// INSECURE
contract Auction is IAuction {
    address public currentLeader;
    uint public highestBid;

    function bid() override external payable {
        require(msg.value > highestBid);

        payable(currentLeader).transfer(highestBid); // Refund the old leader, if it fails then revert
        
        currentLeader = msg.sender;
        highestBid = msg.value;
    }
}