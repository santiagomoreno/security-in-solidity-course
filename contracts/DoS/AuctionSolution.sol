// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./IAuction.sol";

// INSECURE
contract AuctionSolution is IAuction {
    address public highestBidder;
    uint public highestBid;
    mapping(address => uint) public refunds;

    function bid() override external payable {
        require(msg.value >= highestBid);
 
        if (highestBidder != address(0)) {
            refunds[highestBidder] += highestBid;
        }
 
        highestBidder = msg.sender;
        highestBid = msg.value;
    }

    function withdrawRefund() external payable {
        uint refund = refunds[msg.sender];
        refunds[msg.sender] = 0;

        payable(msg.sender).transfer(refund);
    }
}