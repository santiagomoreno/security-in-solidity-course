// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

interface IAuction{
    function bid() external payable;
}