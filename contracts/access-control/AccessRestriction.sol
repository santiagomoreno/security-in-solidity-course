// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract AccessRestriction {

    address public owner;

    constructor(){
        owner = msg.sender;
    }
    
    function changeOwner(address _owner) public {
        owner = _owner;
    }

}