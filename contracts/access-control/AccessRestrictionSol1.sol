// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract AccessRestrictionSol1 {

    address public owner = msg.sender;
    
    modifier onlyOwner() {
        require(msg.sender == owner, "just owner contract");
        _;
    }
    
    function changeOwner(address _newOwner) public onlyOwner() {
        owner = _newOwner;
    }

}