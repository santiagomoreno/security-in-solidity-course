// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";

contract AccessRestrictionSol2 is Ownable {

    uint256 public amount;
        
    function changeOwner() public payable onlyOwner() {
        amount = amount + msg.value;
    }
}