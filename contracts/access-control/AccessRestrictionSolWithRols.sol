// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract AccessRestrictionSolWithRols {

    address public owner;
    address[] public admins ;
    uint public index;

    constructor(){
        owner =  msg.sender;
    }
    
    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }

    modifier onlyAdmin() {
        require(checkAdmin());
        _;
    }

    function checkAdmin() private view returns(bool){
        for(uint i=0; i < admins.length; i++){
            if(msg.sender == admins[i])return true;
        }
        return false;
    }
    
    function addAdmin(address _admin) public onlyOwner() {
        admins[index] = _admin;
        index++;
    }

    function changeIndex(uint _index) public onlyAdmin(){
        index = _index;
    }
}