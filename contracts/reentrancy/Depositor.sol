// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";

interface ISavingsAccount {
    function deposit() external payable;
    function withdraw() external;
}

contract Depositor is Ownable {

    ISavingsAccount public immutable savingsContract;

    constructor(address savingsContractAddress) {
        savingsContract = ISavingsAccount(savingsContractAddress);
    }

    function depositToSavingsAccount() external payable onlyOwner {
        savingsContract.deposit{value: msg.value}();
    }

    function withdrawFromSavingsAccount() external onlyOwner {
        savingsContract.withdraw();
    }
    
    receive() external payable {
    // payable(owner()).transfer(address(this).balance);
    }
}

