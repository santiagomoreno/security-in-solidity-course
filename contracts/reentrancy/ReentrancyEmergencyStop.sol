// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Address.sol";
import "@openzeppelin/contracts/security/Pausable.sol";

contract ReentrancyEmergencyStop is Ownable, Pausable  {
    using Address for address payable;
  
    mapping (address => uint256) public balanceOf;

    function deposit() external payable whenNotPaused {
        balanceOf[msg.sender] += msg.value;
    }

    function withdraw() external whenNotPaused {
        uint256 amountDeposited = balanceOf[msg.sender];

        payable(msg.sender).sendValue(amountDeposited);

        balanceOf[msg.sender] = 0;
    }

    function pauseContract() external onlyOwner {
        _pause();
    }

     function unpauseContract() external onlyOwner {
        _unpause();
    }
}