// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/utils/Address.sol";
import "hardhat/console.sol";

contract ReentrancyVictim {

    using Address for address payable;

    mapping (address => uint256) public balanceOf;

    function deposit() external payable {
        balanceOf[msg.sender] += msg.value;
    }

    // console.log("amountDeposited: ", amountDeposited);
    // console.log("Contract's balance: ", address(this).balance);

    function withdraw() external {
        require(balanceOf[msg.sender] > 0, "There are nothing to withdraw");
        
        uint256 amountDeposited = balanceOf[msg.sender];
        
        payable(msg.sender).sendValue(amountDeposited);

        balanceOf[msg.sender] = 0;
    }
}