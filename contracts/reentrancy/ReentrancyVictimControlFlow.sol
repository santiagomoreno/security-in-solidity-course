// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/utils/Address.sol";

contract ReentrancyVictimControlFlow {

    using Address for address payable;

    mapping (address => uint256) public deposits;

    function deposit() external payable {
      deposits[msg.sender] += msg.value;
    }

    function withdraw() external haveAmount() {
        uint256 amountDeposited = deposits[msg.sender];
        deposits[msg.sender] = 0;

        payable(msg.sender).sendValue(amountDeposited);
    }

    modifier haveAmount(){
        require(deposits[msg.sender] > 0, "You dont have balance");
        _;
    }
}