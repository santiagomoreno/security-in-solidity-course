// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
 
import "@openzeppelin/contracts/access/Ownable.sol";

contract Vault is Ownable{
  bytes32 private password;
  uint256 public totalBalance;
 
  constructor(bytes32 _password){
    password = _password;
  }

  modifier isUnlocked(bytes32 _password){
      require(password == _password, "the password is incorrect");
      _;
  }
  
  function deposit() external payable onlyOwner() {
    totalBalance += msg.value;
  }

  function withdraw(bytes32 _password) external isUnlocked(_password){
      totalBalance = 0;
      payable(msg.sender).transfer(address(this).balance);
  }

}
