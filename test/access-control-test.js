const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("AccessControl", function() {

  let accessRestriction, accessRestriction1, deployer, attacker;

  beforeEach(async function(){
    [deployer, attacker, anotherAc] = await ethers.getSigners();
    
    const AccessRestriction = await ethers.getContractFactory("AccessRestriction", deployer);
    accessRestriction = await AccessRestriction.deploy();

    const AccessRestriction1 = await ethers.getContractFactory("AccessRestrictionSol1", deployer);
    accessRestriction1 = await AccessRestriction1.deploy();

  });

  it("basic case", async function () {
    await accessRestriction.connect(attacker).changeOwner(attacker.address);

    expect(await accessRestriction.owner()).to.eq(attacker.address);
  });

  it("only owner case", async function () {    
    await expect(
        accessRestriction1.connect(attacker).changeOwner(attacker.address)
    ).to.be.revertedWith("just owner contract");
  });
});