const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("DoS", function() {

  let auction, auctionAttacker, deployer, attacker;

  beforeEach(async function(){
    [deployer, attacker, anotherAc] = await ethers.getSigners();
    
    const Auction = await ethers.getContractFactory("Auction", deployer);
    auction = await Auction.deploy();

    const AuctionAttacker = await ethers.getContractFactory("AuctionAttacker", attacker);
    auctionAttacker = await AuctionAttacker.deploy(auction.address);
  });

  it("attack", async function () {
    await auctionAttacker.dosAttack({value: ethers.utils.parseEther('1')});

    await expect(
        auction.connect(anotherAc).bid({value:ethers.utils.parseEther('2')})
      ).to.be.revertedWith("Your lose");
  });
});