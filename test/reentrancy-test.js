const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("reentrancy", function () {
  let reentrancyVictim, reentrancyAttacker, deployer, attacker, deployerTwo, attackerTwo;

  beforeEach(async function () {
    //explicar como funciona el getSigners
    [deployer, attacker, deployerTwo, attackerTwo] = await ethers.getSigners();

    const ReentrancyVictim = await ethers.getContractFactory("ReentrancyVictim", deployer);
    reentrancyVictim = await ReentrancyVictim.deploy();

    await reentrancyVictim.deposit({ value: ethers.utils.parseEther("100") });

    const ReentrancyAttacker = await ethers.getContractFactory("ReentrancyAttacker", attacker);
    reentrancyAttacker = await ReentrancyAttacker.deploy(reentrancyVictim.address);
  });

  it("deposit test", async function () {
    expect(await reentrancyVictim.balanceOf(deployer.address)).to.eq(ethers.utils.parseEther("100"));
  });

  it("reentrancy attack test", async function () {
    let initialBalanceAttacker = await ethers.provider.getBalance(attacker.address);
    let initialBalanceVictim = await ethers.provider.getBalance(reentrancyVictim.address);
    console.log("Contract's initial balance: ", ethers.utils.formatEther(initialBalanceVictim.toString()));
    console.log("Attacker's initial balance: ", ethers.utils.formatEther(initialBalanceAttacker.toString()));

    await reentrancyAttacker.attack({ value: ethers.utils.parseEther("10") });

    let finalBalanceAttacker = await ethers.provider.getBalance(attacker.address);
    let finalBalanceVictim = await ethers.provider.getBalance(reentrancyVictim.address);
    console.log("Contract's final balance: ", ethers.utils.formatEther(finalBalanceVictim.toString()));
    console.log("Attacker's final balance: ", ethers.utils.formatEther(finalBalanceAttacker.toString()));

    expect(await ethers.provider.getBalance(reentrancyVictim.address)).to.eq("0");
  });

  it("pausable contract test", async function () {
    const ReentrancyVictim = await ethers.getContractFactory("ReentrancyEmergencyStop", deployerTwo);
    let reentrancyESVictim = await ReentrancyVictim.deploy();

    await reentrancyESVictim.deposit({ value: ethers.utils.parseEther("100") });

    const ReentrancyAttacker = await ethers.getContractFactory("ReentrancyAttacker", attackerTwo);
    let reentrancyESAttacker = await ReentrancyAttacker.deploy(reentrancyESVictim.address);

    // En un momento, me entero que este contrato tiene una vulnerabilidad, then
    await reentrancyESVictim.pauseContract();

    //ataco y espero que revierta
    await expect(reentrancyESAttacker.attack({ value: ethers.utils.parseEther("10") })).to.be.revertedWith("Pausable: paused");
  });
});
