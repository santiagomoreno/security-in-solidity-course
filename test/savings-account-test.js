const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("savings-account", function () {

  let simpleSavingsAccount, depositor, deployer, user;

  beforeEach(async function(){
    //explicar porque hay que hacer las funciones asyncronas
    [deployer, user] = await ethers.getSigners();

    const SavingsAccount = await ethers.getContractFactory("SavingsAccount", deployer);
    //explicar que en el deploy van los valores del constructor
    simpleSavingsAccount = await SavingsAccount.deploy();

    const Depositor = await ethers.getContractFactory("Depositor", deployer);
    depositor = await Depositor.deploy(simpleSavingsAccount.address);
  });
   
  it("we could deposit", async function () {
    await simpleSavingsAccount.connect(user).deposit({value: ethers.utils.parseEther('1')});

    expect(await simpleSavingsAccount.balanceOf(user.address)).to.eq(ethers.utils.parseEther('1'));
  });

  it("we could withdraw", async function () {
    await simpleSavingsAccount.connect(user).deposit({value: ethers.utils.parseEther('1')});
    await simpleSavingsAccount.connect(user).withdraw();

    expect(await simpleSavingsAccount.balanceOf(user.address)).to.eq(ethers.utils.parseEther('0'));
  });

  it("we could deposit from Depositor", async function () {
    await depositor.connect(deployer).depositToSavingsAccount({value: ethers.utils.parseEther('1')});

    expect(ethers.utils.parseEther('1')).to.eq(await simpleSavingsAccount.balanceOf(depositor.address));
  });

  it("we could withdraw from Depositor", async function () {
    await depositor.connect(deployer).depositToSavingsAccount({value: ethers.utils.parseEther('1')});
    await depositor.connect(deployer).withdrawFromSavingsAccount();

    expect(ethers.utils.parseEther('0')).to.eq(await simpleSavingsAccount.balanceOf(depositor.address));
  });
});