const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Underflow", function () {

  let underflowVictim, deployer, attacker, anotherAc;

  beforeEach(async function(){
    //explicar porque hay que hacer las funciones asyncronas
    [deployer, attacker, anotherAc] = await ethers.getSigners();

    const UnderflowVictim = await ethers.getContractFactory("UnderflowVictim", deployer);
    //explicar que en el deploy van los valores del constructor
    underflowVictim = await UnderflowVictim.deploy(ethers.utils.parseEther('1000'));

    //explicar bien cuando usar el address puro o cuando poner .address(ej: deployer o deployer.address)
    await underflowVictim.transfer(attacker.address, ethers.utils.parseEther('10'));
  });

  it("simple transfer", async function () {
        await underflowVictim.connect(attacker).transfer(anotherAc.address, ethers.utils.parseEther('8'));
        
        expect(await underflowVictim.balanceOf(attacker.address)).to.eq(ethers.utils.parseEther('2'));
        expect(await underflowVictim.balanceOf(anotherAc.address)).to.eq(ethers.utils.parseEther('8'));
  });
   
  it("transfer with more amount of the msg.sender has", async function () {
    //explicar como usar un contrato desde otra address (.connect())
        await underflowVictim.connect(attacker).transfer(anotherAc.address, ethers.utils.parseEther('11'));

        //entender que devuelven valores BigNumber, como manejarlos
        //console.log(await underflowVictim.balanceOf(attacker.address));
        //console.log((await underflowVictim.balanceOf(attacker.address)).toString())
        
        const overflowBalance = (await underflowVictim.balanceOf(attacker.address)) > ethers.utils.parseEther('100'); 
        expect(overflowBalance).to.eq(true);
  });
});